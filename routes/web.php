<?php

use App\Http\Controllers\FormController;
use App\Http\Controllers\PublicController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, "Home"]) ->name("Homepage");

Route::get('/servizi', [PublicController::class, "Servizi"]) ->name("Services");

Route::get('/dettagliServizi/{title}', [PublicController::class, "dettaglioServizi"]) ->name("dettServizi");

Route::get('/chisiamo', [PublicController::class, "Chisiamo"]) ->name("HowTo");

Route::get('/contattaci', [FormController::class, "form"])->name("form");

Route::post('/contattaci/submit', [FormController::class, "store"])->name("store");

Route::get('/grazie',[FormController::class, "thanks"])->name("ThankPage");

Route::get('/tutti-i-contatti', [FormController::class, "index"])->name('allContacts');