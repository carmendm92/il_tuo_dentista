<x-layout title={{$title}}>
    <div class="container">
    <div class="row justify-content-center">
        @foreach ($contacts as $contact)
            <div class="col-4 p-4 m-4 border border-primary text-center shadow-lg">
                <h4>{{$contact->name}}</h4>
                <h6>{{$contact->email}}</h6>
                <p>{{$contact->message}}</p>
            </div>
        @endforeach
    </div>
</div>
</x-layout>