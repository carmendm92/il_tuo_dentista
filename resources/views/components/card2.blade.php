<div class="row m-4">
    <div class="col-12 col-lg-4 d-flex justify-content-center align-items-center"> 
        <a href="{{$route ?? '' }}" class="btn titoloServizi">{{$titoloArticolo}}</a>
    </div> 
    <div class="col-12 col-lg-3 m-4 p-2 mx-4 border border-primary d-flex justify-content-center align-items-center">
        <img class="imgservizi img-fluid" src="{{$imagine ?? '' }}" alt="">
    </div>
    <div class="col-12 col-lg-3 m-4 p-2 text-center border border-primary d-flex justify-content-center align-items-center">
        <p>{{$anteprima}}</p> 
    </div>
 </div>