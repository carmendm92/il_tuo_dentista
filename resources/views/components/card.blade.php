<div class="border border-primary" style="">
    <div class="card-body text-center">
        <img src="{{$imagine ?? '' }}" class="card-img-top" alt="">
        <p> <i class="{{$icona ?? ''}} fa-3x"></i></p>
        <h5 class="card-title">{{$nome}}, {{$cognome}}</h5>
        <p class="card-text">{{$specializzazione}}</p>
        <p class="card-text">{{$anniEsperienza}}</p>
        <p class="card-text">{{$biografia}}</p>
    </div>
  </div>