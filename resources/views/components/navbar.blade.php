
<nav class="navbar back-nav navbar-expand-sm fixed-top navbar-light border border-secondary shadow-lg p-3 mb-5 rounded">
    <div class="container-fluid">
      <a class="navbar-brand" href="{{route('Homepage')}}"><i class="fa-solid fa-tooth fa-2x text-white"></i></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
        <ul class="navbar-nav mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link active text-white" aria-current="page" href="{{route('Homepage')}}">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white" href="{{route('Services')}}">Servizi</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white" href="{{route('HowTo')}}">Chi Siamo</a>
          </li><li class="nav-item">
            <a class="nav-link text-white" href="{{route('form')}}">Contattaci</a>
          </li>
        </li><li class="nav-item">
          <a class="nav-link text-white" href="{{route('allContacts')}}">Contatti</a>
        </li>
        </ul>
      </div>
    </div>
  </nav>