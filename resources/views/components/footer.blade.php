<footer class="container-fluid smile ">
    
        <div class="mt-4 p-0 d-flex align-items-end justify-content-center">
                    <div class="row h-100 text-white">

                        {{-- icone --}}
                        <div class="col-12 col-md-6 my-4">
                            <div class="d-flex justify-content-center container-icon-footer">
                              <div class="container-icon d-flex ">
                                <a class="nav-link" href="#">
                                  <div class="footer-circular-link">
                                    <i class="fa-brands fa-facebook-f link-footer fa-2x"></i>
                                  </div>
                                </a>
                                <a class="nav-link" href="#">
                                  <div class="footer-circular-link">
                                    <i class="fa-brands fa-twitter link-footer fa-2x"></i>
                                  </div>
                                </a>
                                <a class="nav-link" href="#">
                                  <div class="footer-circular-link">
                                    <i class="fa-brands fa-pinterest-p link-footer fa-2x"></i>
                                  </div>
                                </a>
                                <a class="nav-link" href="#">
                                  <div class="footer-circular-link">
                                    <i class="fa-brands fa-instagram link-footer fa-2x"></i>
                                  </div>
                                </a>
                              </div>
                            </div>
                            
                            <p class="text-center">We would love to hear from you <a class="text-white"
                                href="mailto:carmendm92@gmail.com">carmendm92@gmail.com</a> </p>
                        </div>

                      {{-- About --}}
                        <div class="col-12 col-md-6 text-center p-5">
                            <h3>
                                About Il tuo Dentista.it
                            </h3>
                            <p>
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit architecto in dolor
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit architecto in dolor
                            </p>
                        </div>

                        {{-- coopiright --}}
                      <div class="col-12 d-flex justify-content-center p-2 pb-0">
                        <p class="text-center">Copyright © <strong>Carmen Di Mauro</strong> <br>
                          Distributed by Aulab</p>
                      </div>
                    </div>
        </div>
</footer>