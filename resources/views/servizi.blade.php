<x-layout
    title={{$title}}
>
   <div class="container-fluid">
       @foreach ($services as $servizio)
       <x-card2
           titoloArticolo="{{$servizio['titolo']}}"
           imagine="{{$servizio['img']}}"
           anteprima="{{$servizio['articolo']}}"
           route="{{route('dettServizi', ['title'=>$servizio['titolo']])}}"
           >
       </x-card2>
       @endforeach
   </div>
</x-layout>