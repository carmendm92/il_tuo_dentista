<x-layout title={{$title}}>

    <div class="container">
        <div class="row">
            <div class="col-12 justify-content-center container-form text-white">
                <h2 class="text-center ">Grazie per averci contattato, ti risponderemo al più presto.</h2>
            </div>
            <div class="col-12 text-center">
                <a href="{{route('Homepage')}}" class="btn btncard2">Torna alla home</a>
           </div>
        </div>
    </div>

</x-layout>