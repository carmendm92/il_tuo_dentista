<x-layout title={{$title}}>
    <div class="container-fluid">
        <div class="row align-items-center justify-content-center">
            <div class="col 12 col-sm-4 p-4">
                 <p>{{$services['articolo']}}</p>
            </div> 
            <div class="col 12 col-sm-4">
                 <img src="{{$services['img']}}" class="img-fluid p-4" alt="">
            </div>  
            <div class="col-12 text-center">
                <a href="{{route('Services')}}" class="btn btncard2">Indietro</a>
           </div>  
            
        </div>
    </div>
    
    
</x-layout>