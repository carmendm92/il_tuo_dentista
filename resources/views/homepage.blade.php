<x-layout
title={{$title}}>
    
    <div class="container-fluid">
        <div class="row mb-4 align-items-center flex-column flex-xl-row justify-content-xl-end">

            @foreach ($card as $cards)

                <div class="col-12 col-xl-4 mt-2 position-relative d-flex justify-content-center">
                    <div class="first_card">
                        <img class="imgcard" src="{{$cards['img']}}" alt="">
                    </div>
                </div>

            @endforeach
        </div>
            
        <div class="row justify-content-start m-4">
            @foreach ($card as $cards)
                <div class="col-12 col-md-4 container-text m-4"> 
                    <p class="text-center text-white p-4 fw-bold">
                        {{$cards['description']}}
                    </p>
                </div>
            @endforeach   
        </div>            
    </div>    
</x-layout> 