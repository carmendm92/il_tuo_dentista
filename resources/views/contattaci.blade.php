<x-layout title={{$title}}>


<div class="container">
  <div class="row justify-content-center align-items-center flex-wrap p-4">

    <div class="col-8">
      @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
    </div>


    <div class="col-12 col-md-8">
      <form method="post" action="{{route('store')}}" class="container-form text-white">
      @csrf
        <div class="mb-3">
          <label for="exampleInputname" class="form-label">Nome e Cognome</label>
          <input type="text" class="form-control border-primary" id="exampleInputname"name="name" value="{{old('name')}}">
        </div>
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">Email</label>
          <input type="text" class="form-control border-primary" id="exampleInputEmail1" aria-describedby="emailHelp" name="email" value="{{old('email')}}">
        </div>
        <div class="mb-3">
          <label for="exampleFormControlTextarea1" class="form-label">Example textarea</label>
          <textarea class="form-control border-primary" id="exampleFormControlTextarea1" name="message" rows="3">{{old('message')}}</textarea>
        </div>
        <div class="text-center mt-4">
          <button type="submit" class="btn btncard2 ">Invia</button>
        </div>
      </form>
      
    </div>
  </div>
</div>




</x-layout>