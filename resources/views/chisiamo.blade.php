<x-layout
title={{$title}}>
<section class="container">
    <div class="row align-items-center justify-content-center">
        @foreach ($dottor as $dottore)
            <div class="col-3 p-2 mx-4" style="width: 18rem">
                <x-card
                icona="{{$dottore['icona']}}"
                nome="{{$dottore['name']}}"
                cognome="{{$dottore['surname']}}"
                anniEsperienza="{{$dottore['experience']}}"
                biografia="{{$dottore['biography']}}"
                specializzazione="{{$dottore['specialization']}}"
                {{-- route="{{route('Detail', ['title'=>$articolo['titolo']])}}" --}}
                />
            </div>
        @endforeach
    </div>
</section>
</x-layout>