<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function Home() {
        $title = "Il tuo dentista";
        
        $cards = [
            ['img'=>'/img/internal_studio.jpg', 'description'=>'Le nostre strutture all\'avanguardia Lorem ipsum dolor sit amet consectetur adipisicing elit. 
            Sint, amet eveniet voluptates, odio adipisci nam veniam esse nisi voluptatibus saepe, rerum 
            ullam iure sapiente consectetur quos perferendis iste placeat dolorum!,
             Lorem, ipsum dolor sit amet consectetur adipisicing elit. Obcaecati labore accusantium 
             reiciendis ad odit fugit corrupti necessitatibus assumenda culpa animi ipsa aliquid enim 
             perspiciatis autem tenetur soluta dolor, quos numquam!'],
            ['img'=>'/img/reception.jpg', 'description'=>'Il nostro team di professionisti qualificati Lorem ipsum dolor sit amet consectetur adipisicing elit. 
            Sint, amet eveniet voluptates, odio adipisci nam veniam esse nisi voluptatibus saepe, rerum 
            ullam iure sapiente consectetur quos perferendis iste placeat dolorum!,
             Lorem, ipsum dolor sit amet consectetur adipisicing elit. Obcaecati labore accusantium 
             reiciendis ad odit fugit corrupti necessitatibus assumenda culpa animi ipsa aliquid enim 
             perspiciatis autem tenetur soluta dolor, quos numquam!']
        ];

        return view('homepage', compact("title"), ["card"=> $cards]);
    }
    
    public function Servizi(){
        $title = "Servizi";

    $services = [

        ['titolo'=>'Scanning 3D', 'articolo'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium porro 
        autem voluptatum ad doloremque numquam aperiam impedit soluta totam expedita
         ea eos iste, veniam provident facilis dolorem at, dolor neque!', 'img'=>'/img/servizi(3).jpg'],

        ['titolo'=>'Chirurgia', 'articolo'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium porro 
        autem voluptatum ad doloremque numquam aperiam impedit soluta totam expedita
        ea eos iste, veniam provident facilis dolorem at, dolor neque!', 'img'=>'/img/servizi.jpg'],

        ['titolo'=>'Pacchetto prevenzione', 'articolo'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium porro 
        autem voluptatum ad doloremque numquam aperiam impedit soluta totam expedita
        ea eos iste, veniam provident facilis dolorem at, dolor neque!', 'img'=>'/img/servizi(2).jpg'],

        ['titolo'=>'Dayospital', 'articolo'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium porro 
        autem voluptatum ad doloremque numquam aperiam impedit soluta totam expedita
        ea eos iste, veniam provident facilis dolorem at, dolor neque!', 'img'=>'/img/servizi(4).jpg']
    ];

        return view ('servizi', compact("title"), ["services"=>$services]);
    }
    
    public function dettaglioServizi($title){

        $services = [

            ['titolo'=>'Scanning 3D', 'articolo'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium porro 
            autem voluptatum ad doloremque numquam aperiam impedit soluta totam expedita
             ea eos iste, veniam provident facilis dolorem at, dolor neque!', 'img'=>'/img/servizi(3).jpg'],
    
            ['titolo'=>'Chirurgia', 'articolo'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium porro 
            autem voluptatum ad doloremque numquam aperiam impedit soluta totam expedita
            ea eos iste, veniam provident facilis dolorem at, dolor neque!', 'img'=>'/img/servizi.jpg'],
    
            ['titolo'=>'Pacchetto prevenzione', 'articolo'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium porro 
            autem voluptatum ad doloremque numquam aperiam impedit soluta totam expedita
            ea eos iste, veniam provident facilis dolorem at, dolor neque!', 'img'=>'/img/servizi(2).jpg'],
    
            ['titolo'=>'Dayospital', 'articolo'=>'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium porro 
            autem voluptatum ad doloremque numquam aperiam impedit soluta totam expedita
            ea eos iste, veniam provident facilis dolorem at, dolor neque!', 'img'=>'/img/servizi(4).jpg']
        ];

        foreach ($services as $item) {
            if ($item["titolo"] == $title) {
                return view("dettaglioServiziCard",compact("title"), ["services"=>$item]);
            }
        }
    }

    public function ChiSiamo(){

        $doctors =[
            ['name'=>'Paola', 'surname'=>'Gialli', 'experience'=>'3 anni', 'specialization'=>'Radiologo', 'biography'=>'Lorem ipsum dolor 
            sit amet consectetur adipisicing elit. Omnis deserunt aut veniam mollitia aspernatur dolore iure consequuntur assumenda eligendi,
            maiores, quae, est cupiditate nobis doloremque reiciendis dolores facere illum possimus.', 'icona'=>'fa-solid fa-user-doctor text-danger'],

            ['name'=>'Luigi', 'surname'=>'Rossi', 'experience'=>'10 anni', 'specialization'=>'Chirurgo ', 'biography'=>'Lorem ipsum dolor 
            sit amet consectetur adipisicing elit. Omnis deserunt aut veniam mollitia aspernatur dolore iure consequuntur assumenda eligendi,
             maiores, quae, est cupiditate nobis doloremque reiciendis dolores facere illum possimus.', 'icona'=>'fa-solid fa-user-doctor text-primary'],
      
            ['name'=>'Mario', 'surname'=>'Bianchi', 'experience'=>'5 anni', 'specialization'=>'Endodentista', 'biography'=>'Lorem ipsum dolor 
                sit amet consectetur adipisicing elit. Omnis deserunt aut veniam mollitia aspernatur dolore iure consequuntur assumenda eligendi,
                maiores, quae, est cupiditate nobis doloremque reiciendis dolores facere illum possimus.', 'icona'=>'fa-solid fa-user-doctor text-primary'],

            ['name'=>'Giacomo', 'surname'=>'Verdi', 'experience'=>'9 anni', 'specialization'=>'Anestesia dentale', 'biography'=>'Lorem ipsum dolor 
                sit amet consectetur adipisicing elit. Omnis deserunt aut veniam mollitia aspernatur dolore iure consequuntur assumenda eligendi,
                maiores, quae, est cupiditate nobis doloremque reiciendis dolores facere illum possimus.', 'icona'=>'fa-solid fa-user-doctor text-primary'],

            ['name'=>'Laura', 'surname'=>'Neri', 'experience'=>'8 anni', 'specialization'=>'Ortodentista', 'biography'=>'Lorem ipsum dolor 
            sit amet consectetur adipisicing elit. Omnis deserunt aut veniam mollitia aspernatur dolore iure consequuntur assumenda eligendi,
            maiores, quae, est cupiditate nobis doloremque reiciendis dolores facere illum possimus.', 'icona'=>'fa-solid fa-user-doctor text-danger'],

            ['name'=>'Alfredo', 'surname'=>'Capozzi', 'experience'=>'8 anni', 'specialization'=>'Protesista', 'biography'=>'Lorem ipsum dolor 
                sit amet consectetur adipisicing elit. Omnis deserunt aut veniam mollitia aspernatur dolore iure consequuntur assumenda eligendi,
                maiores, quae, est cupiditate nobis doloremque reiciendis dolores facere illum possimus.', 'icona'=>'fa-solid fa-user-doctor text-primary']
        ];

        $title = "Chi Siamo";
        return view('chisiamo', compact("title"), ["dottor"=> $doctors]);
    }
   
}