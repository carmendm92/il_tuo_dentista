<?php

namespace App\Http\Controllers;


use App\Models\Contact;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use App\Http\Requests\StoreRequest;
use Illuminate\Support\Facades\Mail;

class FormController extends Controller
{
    public function form(){
        $title ="Contattaci";
        return view('contattaci', compact("title"));
    }

    public function store(StoreRequest $req){

        $contact = Contact::create([
            'email'=>$req->email,
            'name'=>$req->name,
            'message'=>$req->message

        ]); 
        
        Mail::to($contact->email)->send(new ContactMail($contact));

        return redirect (route ('ThankPage'));
    }

    public function thanks(){
        $title ="Grazie";
        return view('thankPage', compact("title"));
    }

    public function index(){
        $title="Tutti i tui contatti";
        $contacts = Contact::all();
        return view('allContacts', compact ("contacts", "title"));
    }
}

